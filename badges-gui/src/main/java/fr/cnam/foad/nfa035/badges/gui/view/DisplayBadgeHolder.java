package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

public class DisplayBadgeHolder {

    private static DisplayBadgeHolder INSTANCE;

    private DigitalBadge displayedBadge;

    private DisplayBadgeHolder() {
    }

    public static DisplayBadgeHolder getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DisplayBadgeHolder();
        }

        return INSTANCE;
    }

    /**
     * getter pour DisplayBadge
     * @return
     */
    public DigitalBadge getDisplayedBadge() {
        return displayedBadge;
    }

    /**
     * setter pour DisplayBadge
     * @param displayedBadge
     */
    public void setDisplayedBadge(DigitalBadge displayedBadge) {
        this.displayedBadge = displayedBadge;
    }
}
