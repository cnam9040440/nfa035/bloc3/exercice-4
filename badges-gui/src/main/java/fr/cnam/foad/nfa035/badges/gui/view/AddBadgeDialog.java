package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Commentez-moi
 */
@Component("AddBadge")
@Order(value = 4)
public class AddBadgeDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JFileChooser fileChooser;

    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;

    private DigitalBadge badge;

    @Autowired
    private AddBadgeDialogController addBadgeDialogController;

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }
    private DirectAccessBadgeWalletDAO dao;

    public void setCaller(BadgeWalletGUI caller) {
        this.caller = caller;
    }
    @Autowired
    private BadgeWalletGUI caller;

    /**
     * method faisant apparitre le AddBadge dialogue
     */
    public AddBadgeDialog() {

        fileChooser.setFileFilter(new FileNameExtensionFilter("Petites images *.jpg,*.png,*.jpeg,*.gif", "jpg","png","jpeg","gif"));
        fileChooser.addPropertyChangeListener(this);
        codeSerie.addPropertyChangeListener(this);
        dateDebut.addPropertyChangeListener(this);
        dateFin.addPropertyChangeListener(this);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * action lorsque l'on clique sur le bouton ok
     */
    private void onOK() {
        this.addBadgeDialogController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", AddBadgeDialogController.class);
       addBadgeDialogController.delegateOnOk(caller);
        dispose();
    }

    /**
     * action lorsque l'on clique sur le bouton cancel
     */
    private void onCancel() {
        dispose();
    }

    /**methode main
     * @param args
     */
    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setIconImage(dialog.getIcon().getImage());
        dialog.setVisible(true);
        System.exit(0);
    }

    /**
     * permet de récupérer un icon
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * {@inheritDoc}
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (addBadgeDialogController.validateForm(this)){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }
    }


    /**
     * getter codeSerie
     * @return
     */
    public JTextField getCodeSerie() {
        return codeSerie;
    }

    /**
     * getter FileChooser
     * @return
     */
    public JFileChooser getFileChooser() {
        return fileChooser;
    }

    /**
     * getter dateDebut
     * @return
     */
    public JXDatePicker getDateDebut() {
        return dateDebut;
    }

    /**
     * getter dateFin
     * @return
     */
    public JXDatePicker getDateFin() {
        return dateFin;
    }

    public void postConstruct(){

    }
}
