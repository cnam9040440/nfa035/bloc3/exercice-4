package fr.cnam.foad.nfa035.badges.gui.controller;


import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.components.BadgesPanelFactory;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.apache.commons.lang3.ArrayUtils.addAll;


public class BadgeWalletController {

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";
    public DisplayBadgeHolder displayBadgeHolder;

    public BadgesPanelFactory badgesPanelFactory;

    public DirectAccessBadgeWalletDAO dao;

    public DigitalBadge badge;

    /**
     * permet de deleguer la methode UIComponentsCreation
     * @param badgeWalletGUI
     */
    public void delegateUIComponentsCreation(BadgeWalletGUI badgeWalletGUI){
        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            // 1. Le Model
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
            List<DigitalBadge> tableList = new ArrayList<>();

            tableList.addAll(metaSet);
            badgeWalletGUI.setTableList(tableList);
            // Badge initial
            badge = tableList.get(0);

        } catch (IOException ioException) {
            ioException.printStackTrace();


    }


}

    /**
     * methode permettant d'ajouter un badge
     * @param badge
     */

    public void setAddedBadge(DigitalBadge badge, BadgeWalletGUI badgeWalletGUI) {

        BadgesModel tableModel = badgeWalletGUI.getTableModel();
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1, badgeWalletGUI);

    }

    /**
     * methode permettant de charger un badge
      * @param row
     * @param badgeWalletGUI
     */
    public void loadBadge(int row, BadgeWalletGUI badgeWalletGUI) {
        badgeWalletGUI.getPanelHaut().removeAll();
        badgeWalletGUI.getPanelHaut().revalidate();
        List<DigitalBadge> tableList = badgeWalletGUI.getTableList();
        DigitalBadge displayBadge = tableList.get(row);
        delegateUIManagedFieldsCreation(badgeWalletGUI);
        JTable table1 = badgeWalletGUI.getTable1();
        table1.setRowSelectionInterval(row, row);
        badgeWalletGUI.getPanelHaut().add(badgeWalletGUI.getScrollHaut());
        badgeWalletGUI.getPanelImageContainer().setPreferredSize(new Dimension(256, 256));
        badgeWalletGUI.getScrollHaut().setViewportView(badgeWalletGUI.getPanelImageContainer());

        badgeWalletGUI.getPanelHaut().repaint();
    }

    /**
     * permet de deleguer la metode UIManagedFieldsCreation
     * @param badgeWalletGUI
     */
    public void delegateUIManagedFieldsCreation(BadgeWalletGUI badgeWalletGUI) {
        BadgePanel badgePanel  = new BadgePanel(badge, dao);
        badgePanel.setPreferredSize(new Dimension(256, 256));
        JPanel panelImageContainer = new JPanel();
        panelImageContainer.add(badgePanel);
    }
}
