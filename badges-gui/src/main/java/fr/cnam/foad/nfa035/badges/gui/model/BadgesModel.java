package fr.cnam.foad.nfa035.badges.gui.model;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

public class BadgesModel extends AbstractTableModel {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1944258978183994752L;

    private final String[] entetes = { "ID", "Code Série", "Début", "Fin", "Taille (octets)" };

    public void setBadges(List<DigitalBadge> badges) {
        this.badges = badges;
    }

    public void addBadge(DigitalBadge badge) {
        this.badges.add(badge);
    }

    private List<DigitalBadge> badges;

    public BadgesModel(List<DigitalBadge> badges) {
        super();
        this.badges = badges;
    }

    @Override
    /**
     * reourne le nombre de colonne
     */
    public int getColumnCount() {
        return entetes.length;
    }

    @Override
    /**
     * retourne le nom d'une colonne
     */
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    @Override
    /**
     * retoune le nombre de ligne
     */
    public int getRowCount() {
        return badges.size();
    }

    @Override
    /**
     * retrourne la valeur pour une ligne et une colonne donnée
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {

        case 0:
            // ID dans le Wallet
            return badges.get(rowIndex).getMetadata().getBadgeId();

        case 1:
            // Code de Série
            return badges.get(rowIndex).getSerial();

        case 2:
            // Date d'obtention
            return badges.get(rowIndex).getBegin();

        case 3:
            // Date de péremption
            return badges.get(rowIndex).getEnd();

        case 4:
            // Taille de l'image
            return badges.get(rowIndex).getMetadata().getImageSize();

        default:
            throw new IllegalArgumentException();
        }
    }

    @Override
    /**
     * retoune la classe d'une colonne
      */
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {

        case 0:
            return Integer.class;

        case 1:
            return String.class;

        case 3:
            return Date.class;

        case 2:
            return Date.class;

        case 4:
            return Long.class;

        default:
            return Object.class;
        }
    }

    /**
     * retourne une liste des badges
     * @return
     */
    public List<DigitalBadge> getBadges() {
        return badges;
    }

}