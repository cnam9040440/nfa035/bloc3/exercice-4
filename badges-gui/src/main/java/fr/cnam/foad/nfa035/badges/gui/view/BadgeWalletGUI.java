package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.*;

/**
 * Commentez-moi
 */
@Component("badgeWallet")
@Order(value = 2)

/**
 * classe représentant le badge wallet UI
 */
public class BadgeWalletGUI{

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;
    private BadgePanel badgePanel;
    private BadgesModel tableModel;


    public DigitalBadge getDisplayBadge() {
        return displayBadge;
    }

    private DigitalBadge displayBadge;
    private DirectAccessBadgeWalletDAO dao;
    List<DigitalBadge> tableList;


    @Autowired
    private AddBadgeDialog addBadgeDialog;

    @Autowired
    private BadgeWalletController badgesWalletController;


    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    /**
     * methode faisant apparaitre le badge wallet UI
     */

    public BadgeWalletGUI() {
        addBadgeDialog = new AddBadgeDialog();
        addBadgeDialog.setCaller(this);
        addBadgeDialog.setDao(dao);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addBadgeDialog.pack();
                addBadgeDialog.setLocationRelativeTo(null);
                addBadgeDialog.setIconImage(addBadgeDialog.getIcon().getImage());
                addBadgeDialog.setVisible(true);
            }
        });

        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());
        table1.setModel(tableModel);
        table1.setRowSelectionInterval(0, 0);

        // 2. Les Renderers...
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    loadBadge(row);
                }
            }
        });


    }

    /**
     * méthode main
     * @param args les arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet");
        BadgeWalletGUI gui = new BadgeWalletGUI();
        frame.setContentPane(gui.panelParent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(gui.getIcon().getImage());
        frame.setVisible(true);
    }

    /**
     *permet de récupérer un icon
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * methode permettant de créer un componant
     */
    private void createUIComponents() {
        this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);

        badgesWalletController.delegateUIComponentsCreation(this);
        badgesWalletController.delegateUIManagedFieldsCreation(this);


    }

    /**
     * Créé un Panel
     */

    private void setCreatedUIFields() {
        this.badgePanel = new BadgePanel(displayBadge, dao);
        this.badgePanel.setPreferredSize(new Dimension(256, 256));
        this.panelImageContainer = new JPanel();
        this.panelImageContainer.add(badgePanel);
    }


    /**
     * methode permettant d'ajouter un badge
     * @param badge
     */

    public void setAddedBadge(DigitalBadge badge) {
        this.displayBadge = badge;
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1);
    }

    /**
     * methode permettant de charger un badge
     * @param row
     */
    private void loadBadge(int row){
        panelHaut.removeAll();
        panelHaut.revalidate();

        displayBadge = tableList.get(row);
        setCreatedUIFields();
        table1.setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    }

    /**
     * setter pour panelImageContainer
     * @param panelImageContainer
     */
    public void setPanelImageContainer(JPanel panelImageContainer) {
        this.panelImageContainer = panelImageContainer;
    }

    /**
     * setter pour badgePanel
     * @param badgePanel
     */
    public void setBadgePanel(BadgePanel badgePanel) {
        this.badgePanel = badgePanel;
    }

    /**
     * setter tableList
     * @param tableList
     */
    public void setTableList(List<DigitalBadge> tableList) {
        this.tableList = tableList;
    }

    /**
     * getter pour panelParent
     * @return panelParent
     */
    public Container getPanelParent() {
        return panelParent;
    }

    /**
     * getter pour table1
     * @return table1
     */
    public JTable getTable1() {
        return table1;
    }

    /**
     * getter pour panelImageContainer
     * @return panelImageContainer
     */
    public JPanel getPanelImageContainer() {
        return panelImageContainer;
    }

    /**
     * getter pour scrollHaut
     * @return scrollHaut
     */
    public JScrollPane getScrollHaut() {
        return scrollHaut;
    }

    /**
     * getter pour panelHaut
     * @return panelHaut
     */
    public JPanel getPanelHaut() {
        return panelHaut;
    }

    /**
     * getter pour panelBas
     * @return panelBas
     */
    public JPanel getPanelBas() {
        return panelBas;
    }

    /**
     * getter pour tableModel
     * @return tableModel
     */
    public BadgesModel getTableModel() {
        return tableModel;
    }

    /**
     * getter pour tableList
     * @return tableList
     */
    public List<DigitalBadge> getTableList() {
        return tableList;
    }
}
