package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog;
import fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import java.awt.Window;

public class AddBadgeDialogController {

    public DisplayBadgeHolder displayBadgeHolder;

    public DirectAccessBadgeWalletDAO dao;

    private DigitalBadge badge;

    /**
     * permet la delegation de la methode onOk
     * @param caller
     */
    public void delegateOnOk(BadgeWalletGUI caller){

        try {
            dao.addBadge(badge);
            caller.setAddedBadge(badge);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        //dispose();
    }



    /**
     * permet de vérifier si le form est valide
     * @return un boolean
     */
    public boolean validateForm(AddBadgeDialog addBadgeDialog){
        File image = addBadgeDialog.getFileChooser().getSelectedFile();
        String codeSerieStr = addBadgeDialog.getCodeSerie().getText();
        Date fin = addBadgeDialog.getDateFin().getDate();
        Date debut = addBadgeDialog.getDateDebut().getDate();
        if (codeSerieStr != null && codeSerieStr.trim().length() > 4 && image != null && image.exists()
             && ((fin != null && debut == null)||(fin != null && fin.after(debut)))){
             this.badge = new DigitalBadge(codeSerieStr,debut,fin,null,image);
             return true;
        }
        else{
            return false;
        }
    }
}
